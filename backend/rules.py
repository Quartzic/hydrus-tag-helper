import json
from backend.actions import RULE_ACTION_SHORTCUTS, new_action
from backend.search import Search
from pathlib import Path
from type_checking import require_bool, require_list, require_str

class Rule():
    def __init__(self, file_path: str, data: dict) -> None:
        self.file_path = file_path
        self.uid = require_str("uid", data, required=True) #TODO: Generate UID and store it in the json file.
        self.name = require_str("name", data, "Unnamed Rule")
        self.desc = require_str("desc", data, "No description provided.")
        self.disabled = require_bool("disabled", data)

        try: self.search
        except AttributeError:
            self.search = require_list("search", data, required=True)

        self.search = Search(self.search)

        try: self.actions
        except AttributeError:
            self.actions = []

        self.actions = [new_action(x) for x in self.actions]
        self.custom_actions = [new_action(x) for x in require_list("custom_actions", data, item_type=dict, default=[])]

class TagDisambiguationRule(Rule):
    def __init__(self, file_path: str, data: dict) -> None:
        self.bad_tags = require_list("bad_tags", data, item_type=str, required=True)
        self.alternatives = require_list("alternatives", data, item_type=str, default=[])

        self.actions = []
        for i, alternative in enumerate(self.alternatives):
            self.actions.append({
                "name": "Replace with " + alternative,
                "archetype": "tag_action",
                "shortcut": RULE_ACTION_SHORTCUTS.get(i, ""),
                "resolves": True,
                "remove_tags": self.bad_tags,
                "add_tags": [alternative]
            })

        self.search = [self.bad_tags]

        super().__init__(file_path, data)

class MutualExclusionRule(Rule):
    def __init__(self, file_path: str, data: dict) -> None:
        self.tags = require_list("tags", data, item_type=str, required=True)

        self.search = []
        self.search.append(self.tags)
        for i in range(len(self.tags)):
            l = self.tags.copy()
            l[i] = "-" + l[i]
            self.search.append(l)

        self.actions = []
        for i, tag in enumerate(self.tags):
            bad_tags = self.tags.copy()
            bad_tags.remove(tag)
            self.actions.append({
                "name": "Keep " + tag,
                "archetype": "tag_action",
                "shortcut": RULE_ACTION_SHORTCUTS.get(i, ""),
                "resolves": True,
                "remove_tags": bad_tags,
                "add_tags": [tag]
            })

        super().__init__(file_path, data)

class RequireOneRule(Rule):
    def __init__(self, file_path: str, data: dict) -> None:
        self.search = require_list("search", data, default=[])
        self.tags = require_list("tags", data, item_type=str, required=True)

        self.actions = []
        for i, tag in enumerate(self.tags):
            self.actions.append({
                "name": "Tag " + tag,
                "archetype": "tag_action",
                "shortcut": RULE_ACTION_SHORTCUTS.get(i, ""),
                "resolves": True,
                "add_tags": [tag]
            })

        self.search.extend([f"-{x}" for x in self.tags])

        super().__init__(file_path, data)

class HelicopterParentRule(RequireOneRule):
    def __init__(self, file_path: str, data: dict) -> None:
        data["search"] = [require_str("parent", data, required=True)]
        data["tags"] = require_list("add_children", data, item_type=str, default=[])

        super().__init__(file_path, data)

RULE_TEMPLATES: dict[str, type[Rule]] = {
    "default": Rule,
    "tag disambiguation": TagDisambiguationRule,
    "require one": RequireOneRule,
    "helicopter parent": HelicopterParentRule,
    "mutual exclusion": MutualExclusionRule
}

def load_rules(file_path: str = "./default_rules/") -> list[Rule]:
    file_path: Path = Path(file_path)
    ret: dict[str, Rule] = {}

    def new_rule(file_path: str, data: dict) -> Rule:
        template = require_str("template", data, default="default", lower=True)
        if template not in RULE_TEMPLATES:
            raise ValueError(f'Error with rule defined in {file_path}.\nRule template "{template}"" is not valid. Here\'s a list of supported templates: {RULE_TEMPLATES}')
        return RULE_TEMPLATES[template](file_path, data)

    def add_rule(rule: Rule):
        if rule.uid in ret:
            raise Exception(f"Rule defined in {rule.file_path} has the same uid as rule defined in {ret[rule.uid].file_path}")
        ret[rule.uid] = rule

    if file_path.is_dir():
        for file in file_path.glob('*'):
            for x in load_rules(file):
                add_rule(x)
    elif file_path.is_file() and file_path.suffix == ".json":
        with open(file_path, "r") as file:
            data = json.load(file)
            if type(data) is dict:
                data = [data]
            if type(data) is not list:
                raise ValueError(f'Root element of rule definition in {file_path} is not of type "Dictionary" or "List".')
            for rule_def in data:
                if type(rule_def) != dict:
                    raise ValueError(f'Error with rule defined in {file_path}.\nRule definitions should be of type "Dictionary".')
                if not rule_def.get("disabled", False):
                    rule = new_rule(str(file_path), rule_def)
                    #TODO: store rule somewhere. Then allow the user to enable it from GUI.
                    add_rule(rule)

    return ret.values()