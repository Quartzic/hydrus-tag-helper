import backend.hydrus
import backend.settings
from hydrus_api import APIError, ConnectionError
from qtpy.QtCore import Qt
from qtpy.QtGui import QDesktopServices
from qtpy.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from ui.helper import loadUi

class __BaseDialog():
    _dialog = None
    _ui_file = None

    #TODO: Prevent instantiation of base class(es).

    def __init__(self) -> None:
        if self.dialog is None:
            ui_file = type(self)._ui_file

            if ui_file is None:
                raise RuntimeError(f"You forgot to set the UI file path of {type(self).__name__}, you idiot.")

            type(self)._dialog = loadUi(ui_file, QDialog)
            self._initial_setup()

        self._on_exec()
        if self.dialog.exec() == QDialog.DialogCode.Accepted:
            self._on_confirm()
        else:
            self._on_reject()

    def _initial_setup(self) -> None:
        button_box = self.dialog.findChild(QDialogButtonBox)
        if button_box is not None:
            button_box.helpRequested.connect(self._on_help_requested)

    # TODO: document all these functions.
    def _on_exec(self) -> None:
        pass

    def _on_help_requested(self) -> None:
        pass

    def _on_confirm(self) -> None:
        pass

    def _on_reject(self) -> None:
        pass

    @property
    def dialog(self) -> QDialog:
        return type(self)._dialog


class HydrusConnectDialog(__BaseDialog):
    _ui_file = "ui/templates/dialogs/connect_to_hydrus.ui"

    def _initial_setup(self) -> None:
        super()._initial_setup()

        # This value is hardcoded in the .ui file already, but just in case.
        self.dialog.api_url.setPlaceholderText(backend.hydrus.DEFAULT_API_URL)

        # TODO: Give me a nice icon :)
        connect_button = self.dialog.buttonBox.addButton("Connect", QDialogButtonBox.ApplyRole)
        connect_button.clicked.connect(lambda: self._connect_to_hydrus())
        self.dialog.connect_button = connect_button

        self.dialog.access_key.textChanged.connect(self._update_button_text)

    def _update_button_text(self, text: str) -> None:
        self.dialog.connect_button.setText("Connect" if text else "Request API Key")

    def _on_exec(self) -> None:
        settings = backend.settings.get_settings()

        self.access_key = settings.hydrus_access_key

        self.api_url = settings.hydrus_api_url
        if self.api_url == backend.hydrus.DEFAULT_API_URL:
            self.api_url = ""

        self.auto_reconnect = settings.hydrus_auto_reconnect

        self.dialog.access_key.setText(self.access_key)
        self.dialog.api_url.setText(self.api_url)
        self.dialog.auto_reconnect.setChecked(self.auto_reconnect)
        self._update_button_text(self.access_key)

    def _on_help_requested(self) -> None:
        doc_url = "https://hydrusnetwork.github.io/hydrus/client_api.html"
        if not QDesktopServices.openUrl(doc_url):
            QMessageBox.information(None, "Couldn't open web browser", f"Coudln't open web browser to Hydrus' help page. Here's the URL:\n{doc_url}")

    def _on_confirm(self) -> None:
        self.access_key = self.dialog.access_key.text().strip()
        self.api_url = self.dialog.api_url.text().strip()
        self.auto_reconnect = self.dialog.auto_reconnect.isChecked()

        settings = backend.settings.get_settings()
        settings.hydrus_access_key = self.access_key
        settings.hydrus_api_url = self.api_url
        settings.hydrus_auto_reconnect = self.auto_reconnect
        settings.save_to_file()

    def _connect_to_hydrus(self) -> None:
        key = self.dialog.access_key.text().strip()
        url = self.dialog.api_url.text().strip()

        if not url:
            url = backend.hydrus.DEFAULT_API_URL

        if not key:
            key = self.request_key(url)
            self.dialog.access_key.setText(key)
            return

        try:
            backend.hydrus.client().connect(key, url)
        except backend.hydrus.HydrusConnectionError as e:
            QMessageBox.warning(None, "Connection Failed", str(e))
        else:
            QMessageBox.information(None, "Connection Success", "Connected to Hydrus API succesfully.")

    def request_key(self, api_url: str) -> str:
        #TODO: I was drunk or something. - laure
        try:
            key = backend.hydrus.client().request_api_key(api_url)
        except APIError as e:
            QMessageBox.warning(None, "Action necessary", f"The API responded with the following message: {str(e)}")
        except ConnectionError:
            QMessageBox.warning(None, "Connection Error!", f"Connection Error: There was a problem trying to connect to the API at {api_url}, make sure the URL is correct.")
        except Exception as e:
            QMessageBox.warning(None, "Error!", f"{type(e).__name__}: " + str(e))
        else:
            QMessageBox.information(None, "Connection Success!", "Connection Success: Click OK after you're accepted the permissions on Hydrus.")
            return key
