import backend.hydrus
import qtpy
import ui
import ui.helper
from ui.helper import default_exceptions
from backend.actions import Action, FILE_GLOBAL_ACTIONS
from backend.rules import Rule
from qtpy.QtCore import Signal
from qtpy.QtWidgets import QMessageBox, QPushButton, QWidget

class Widget(QWidget):
    def __init__(self, layout: type[ui.BoxLayout] = ui.VBoxLayout) -> None:
        super().__init__()
        self.setLayout(layout())

    @property
    def layout(self) -> ui.BoxLayout:
        return super().layout()

class RuleList(Widget):
    def update_content(self, rules: list[Rule]) -> None:
        self.layout.remove_all_items()

        client_disconnected = not backend.hydrus.client().connected

        if len(rules) == 0:
            self.layout.add(ui.helper.new_label("(Defined rules will show here)", center=True, font_size=9))
        for rule in rules:
            button_name = self._get_rule_display_name(rule)
            button = QPushButton(button_name)
            button.setSizePolicy(ui.helper.SizePolicy.Preferred, ui.helper.SizePolicy.Minimum)
            button.setDisabled(client_disconnected)
            button.clicked.connect(lambda *_, b=button, r=rule: self._on_rule_button_click(b, r))
            self.layout.add(button)

    def _get_rule_display_name(self, rule: Rule):
        parts = [rule.name]

        if rule.search.has_results():
            parts.append(f"({rule.search.result_count})")
        else:
            parts.append("(?)")

        return ' '.join(parts)

    @default_exceptions
    def _on_rule_button_click(self, button: QPushButton, rule: Rule):
        if rule.search.empty:
            button.setText(self._get_rule_display_name(rule))
            QMessageBox.information(None, "Empty Rule", "This rule gave no results.")
            return
        ui.main_window().go_to_rule_page(rule)

class RuleActions(Widget):
    action_triggered = Signal(Action)

    def __init__(self) -> None:
        super().__init__()

        self.global_action_buttons: list[tuple[Action, QPushButton]] = []
        self.rule_specific_buttons: list[tuple[Action, QPushButton]] = []

        permanent_actions = ui.VBoxLayout()
        permanent_actions.add(ui.helper.new_label("Actions", center=True))

        for action in FILE_GLOBAL_ACTIONS:
            button = self.button_from_action(action)
            self.global_action_buttons.append((action, button))
            permanent_actions.add(button)

        self.rule_actions = ui.VBoxLayout()
        self.layout.add(self.rule_actions)

        self.layout.add(permanent_actions)

    def button_from_action(self, action: Action) -> QPushButton:
        text = action.name
        if action.shortcut:
            text += f" ({action.shortcut})"

        button = QPushButton(text)
        if action.shortcut:
            button.setShortcut(action.shortcut)

        button.clicked.connect(lambda *_, a=action: self.action_triggered.emit(a))

        return button

    def update_buttons(self, inbox: bool, trash: bool, exempt: bool):
        for action, button in [*self.global_action_buttons, *self.rule_specific_buttons]:
            hidden = (
                action.hidden or
                (inbox and action.hidden_if_inbox) or
                (not inbox and action.hidden_if_archive) or
                (trash and action.hidden_if_trash) or
                (exempt and action.hidden_if_exempt) or
                (not exempt and action.hidden_if_not_exempt)
            )
            button.setHidden(hidden)

    def update_content(self, rule_actions: list[Action]) -> None:
        self.rule_actions.remove_all_items()
        self.rule_specific_buttons = []

        if len(rule_actions) > 0:
            self.rule_actions.add(ui.helper.new_label("Rule Actions", center=True))
            for action in rule_actions:
                button = self.button_from_action(action)
                self.rule_specific_buttons.append((action, button))
                self.rule_actions.add(button)

            self.rule_actions.addSpacing(20)

class RuleDesc(Widget):
    def __init__(self) -> None:
        super().__init__()

    def update_content(self, rule: Rule) -> None:
        self.layout.remove_all_items()
        if not rule: return

        self.layout.add(ui.helper.new_label("Rule: " + rule.name))
        desc = ui.helper.new_label(rule.desc, font_size=9)
        self.layout.add(desc)

class TagListWidgetItem(qtpy.QtWidgets.QListWidgetItem):
    def __init__(self, tag: str) -> None:
        super().__init__()
        self.setText(tag)
        self.set_color(backend.hydrus.client().get_tag_color(tag))
        self._tag = tag

    def set_color(self, hex_color: str):
        self.setForeground(qtpy.QtGui.QBrush(qtpy.QtGui.QColor(hex_color)))

    def compare(self, other : "TagListWidgetItem"):
        return backend.hydrus.compare_tags(self._tag, other._tag)

    def __eq__(self, other): return self.compare(other) == 0
    def __ge__(self, other): return self.compare(other) >= 0
    def __le__(self, other): return self.compare(other) <= 0
    def __gt__(self, other): return self.compare(other) > 0
    def __lt__(self, other): return self.compare(other) < 0
