import backend.hydrus
import backend.settings
import hydrus_api
from type_checking import require_bool, require_list, require_str

# Shortcuts for the first 10 custom actions defined by a rule.
# TODO: Make configurable use of shift, or disable auto-shortcuts entirely.
RULE_ACTION_SHORTCUTS = {i - 11: f"{i}"[1:] for i in range(11, 21)}


class Action:
    def __init__(self, data: dict) -> None:
        self.name = require_str("name", data, default="Unnamed Action")
        self.archetype = data["archetype"] #TODO: Enforce the use of new_action to avoid instantiating actions of invalid or non matching archetypes.
        self.shortcut = require_str("shortcut", data)
        if self.shortcut == "":
            self.shortcut = None
        self.resolves = require_bool("resolves", data)

        self.hidden = require_bool("hidden", data)
        self.hidden_if_inbox = require_bool("hidden_if_inbox", data)
        self.hidden_if_archive = require_bool("hidden_if_archive", data)
        self.hidden_if_trash = require_bool("hidden_if_trash", data)
        self.hidden_if_rule = require_bool("hidden_if_rule", data)
        self.hidden_if_no_rule = require_bool("hidden_if_no_rule", data)
        self.hidden_if_exempt = require_bool("hidden_if_exempt", data)
        self.hidden_if_not_exempt = require_bool("hidden_if_not_exempt", data)

    def run(self, file_hash: str) -> bool:
        return True

class TagAction(Action):
    def __init__(self, data: dict) -> None:
        super().__init__(data)

        self.tag_dialog = require_str("tag_dialog", data, lower=True)
        if self.tag_dialog and not self.tag_dialog in ["add", "remove"]:
            raise ValueError(f'Action of archetype "{self.archetype}" only supports "add" or "remove" values for key "tag_dialog".')

        self.add_tags = require_list("add_tags", data, str)
        self.remove_tags = require_list("remove_tags", data, str)
        if not self.tag_dialog and not self.add_tags and not self.remove_tags:
            raise ValueError(f'Action of archetype "{self.archetype}" requires at least one of "add_tags" or "remove_tags" to be a valid list of tags.')

    def run(self, file_hash: str, tags: list[str] = None) -> bool:
        ACTION_ADD = str(hydrus_api.TagAction.ADD)
        ACTION_REMOVE = str(hydrus_api.TagAction.DELETE)

        tag_operation = {}

        if self.add_tags:
            tag_operation[ACTION_ADD] = self.add_tags
        if self.remove_tags:
            tag_operation[ACTION_REMOVE] = self.remove_tags

        if tags:
            op = ACTION_ADD if self.tag_dialog == "add" else ACTION_REMOVE
            if op not in tag_operation:
                tag_operation[op] = []
            tag_operation[op].extend(tags)

        if len(tag_operation) > 0:
            service = backend.settings.get_settings().primary_tag_service
            if service is None:
                return False

            sntatt = {}
            sntatt[service] = tag_operation

            backend.hydrus.client().change_tags(
                hashes=[file_hash],
                service_names_to_actions_to_tags=sntatt)

            return True

        return False

class MoveToTrashAction(Action):
    def run(self, file_hash: str) -> bool:
        backend.hydrus.client().delete_files([file_hash])
        return True

class MoveToInboxAction(Action):
    def run(self, file_hash: str) -> bool:
        backend.hydrus.client().unarchive_files([file_hash])
        return True

class MoveToArchiveAction(Action):
    def run(self, file_hash: str) -> bool:
        backend.hydrus.client().archive_files([file_hash])
        return True

class MarkAsExemptAction(Action):
    pass

class UnmarkAsExemptAction(Action):
    pass

class SkipAction(Action):
    pass

# special action class - when run, does nothing and moves to previous file
class UnskipAction(Action):
    pass


ACTION_ARCHETYPES: dict[str, type[Action]] = {
    "tag_action": TagAction,
    "move_to_trash": MoveToTrashAction,
    "move_to_inbox": MoveToInboxAction,
    "move_to_archive": MoveToArchiveAction,
    "mark_as_exempt": MarkAsExemptAction,
    "unmark_as_exempt": UnmarkAsExemptAction,
    "skip": SkipAction,
    "unskip": UnskipAction
}

def new_action(data: dict) -> Action:
    archetype = require_str("archetype", data, required=True, lower=True)
    if archetype not in ACTION_ARCHETYPES:
        raise ValueError(f'Action archetype "{archetype}"" is not valid. Here\'s a list of supported archetypes: {ACTION_ARCHETYPES.keys()}')
    data["archetype"] = archetype
    return ACTION_ARCHETYPES[archetype](data)

FILE_GLOBAL_ACTIONS = [
    new_action({
        "name": "Quick Tag",
        "archetype": "tag_action",
        "tag_dialog": "add",
        "shortcut": "="
    }),
    new_action({
        "name": "Quick Untag",
        "archetype": "tag_action",
        "tag_dialog": "remove",
        "shortcut": "-"
    }),
    new_action({
        "name": "Move to Trash",
        "archetype": "move_to_trash",
        "hidden_if_trash": True,
        "shortcut": "d"
    }),
    new_action({
        "name": "Move to Inbox",
        "archetype": "move_to_inbox",
        "hidden_if_inbox": True,
        "shortcut": "shift+a"
    }),
    new_action({
        "name": "Move to Archive",
        "archetype": "move_to_archive",
        "hidden_if_archive": True,
        "shortcut": "a"
    }),
    new_action({
        "name": "Mark as Exempt",
        "archetype": "mark_as_exempt",
        "shortcut": "x",
        "resolves": True,
        "hidden_if_no_rule": True,
        "hidden_if_exempt": True
    }),
    new_action({
        "name": "Remove Exemption",
        "archetype": "unmark_as_exempt",
        "shortcut": "shift+x",
        "hidden_if_no_rule": True,
        "hidden_if_not_exempt": True
    }),
    new_action({
        "name": "Skip",
        "archetype": "skip",
        "shortcut": "s",
        "resolves": True,
        "hidden_if_no_rule": True
    }),
    new_action({
        "name": "Go Back",
        "archetype": "unskip",
        "shortcut": "shift+s",
        "hidden_if_no_rule": True
    })
]
