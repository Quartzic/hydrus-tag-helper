import backend.hydrus

class Search():
    def __init__(self, query: list[str, list[str]]) -> None:
        self.query = query
        self.result_files = None

    def has_results(self) -> bool:
        return self.result_files is not None

    def assert_has_results(self) -> None:
        if not self.has_results():
            self.perform_search()

    @property
    def result_count(self) -> int:
        self.assert_has_results()
        return len(self.result_files)

    @property
    def empty(self) -> bool:
        self.assert_has_results()
        return self.result_count == 0

    def _check_index(self, index: int) -> int:
        if index < 0 or index >= self.result_count:
            raise IndexError("Index out of bounds: " + str(index))
        return index

    def perform_search(self, download_metadata_now = False):
        result_ids = backend.hydrus.client().search_files(self.query)
        self.result_files = backend.hydrus.client().get_files_by_id(
            file_ids=result_ids, download_metadata_now=download_metadata_now)

    def get_file(self, index: int) -> backend.hydrus.HydrusFile:
        self.assert_has_results()
        self._check_index(index)
        ret = self.result_files[index]
        return ret

    def invalidate_file_metadata(self, index: int) -> None:
        if not self.has_results():
            return # nothing to invalidate :)
        self.get_file(index).invalidate_cache()
