import json
import typing as T
import os
from backend.hydrus import DEFAULT_API_URL
from copy import deepcopy
from configparser import ConfigParser
from pathlib import Path

class Settings:
    _SECTIONS_TO_KEYS = {
        "HYDRUS": [
            "access_key",
            "api_url",
            "auto_reconnect"
        ],
        "LINTER": [
            "primary_tag_service",
            "secondary_tag_services"
        ],
        "SLIDESHOW": [],
        "TAG_NAMESPACE_COLORS": []
    }

    def __init__(self) -> None:
        self.file_path = Path("config.ini")
        self.config = self._load_from_file()

        self._hydrus_access_key = self._get_str("HYDRUS", "access_key", env_key="HYDRUS_ACCESS_KEY")
        self._hydrus_api_url = self._get_str("HYDRUS", "api_url", env_key="HYDRUS_API_URL")
        self._hydrus_auto_reconnect = self._get_bool("HYDRUS", "auto_reconnect", default=True)

        # TODO: Force the user to specify a tag service in the config. Recommend a dedicated service for the linter.
        # Do not assume 'my tags' as it could lead to accidentally tagging/untagging when learning to use the program.
        self._primary_tag_service = self._get_str("LINTER", "primary_tag_service", env_key="HYDRUS_PRIMARY_TAG_SERVICE", default="my tags")
        self._secondary_tag_services = self._get_str_list("LINTER", "secondary_tag_services", env_key="HYDRUS_SECONDARY_TAG_SERVICES", default=[])

    def _validate_section_key(self, section: str, key: str) -> None:
        if section not in self._SECTIONS_TO_KEYS:
            raise KeyError(f'Settings: Section "{section} is not defined in the config template."')
        if key not in self._SECTIONS_TO_KEYS[section]:
            raise KeyError(f'Settings: Key "{key}" is not defined for section "{section}".')

    # try to load a setting, with this order of priorities:
    # 1. get it from an environment variable
    # 2. get it from the configparser
    # 3. use a default value (fallback)
    def _get_str(self, section: str, key: str, env_key: T.Optional[str] = None, default: str = "") -> str:
        self._validate_section_key(section, key)
        if env_key is not None:
            env_val = os.getenv(env_key)
            if env_val is not None:
                return env_val

        value = self.config.get(section, key, fallback="")
        if value: # "" is considered False
            return value
        return default
    
    def _get_str_list(self, section: str, key: str, env_key: T.Optional[str] = None, default: list[str] = []) -> list[str]:
        self._validate_section_key(section, key)

        # Check if the value is in the environment variable
        if env_key is not None:
            env_val = os.getenv(env_key)
            if env_val is not None:
                loaded_list = json.loads(env_val)
                if isinstance(loaded_list, list):
                    return loaded_list
        
        # Check if the value is in the config file
        value = self.config.get(section, key, fallback="")
        if value:
            loaded_list = json.loads(value)
            if isinstance(loaded_list, list):
                return loaded_list

        # Return the default value otherwise
        return default

    def _get_bool(self, section: str, key: str, env_key: T.Optional[str] = None, default: bool = False) -> bool:
        b = self._get_str(section, key, env_key=env_key).lower()
        if b:
            return (b == "1" or b == "true")
        return default

    def _set_str(self, section: str, key: str, value: T.Optional[str]) -> None:
        self._validate_section_key(section, key)

        if value is None:
            value = ""

        self.config[section][key] = value

    @property
    def hydrus_access_key(self) -> T.Optional[str]:
        if not self._hydrus_access_key:
            return None
        return self._hydrus_access_key

    @property
    def hydrus_api_url(self) -> str:
        if not self._hydrus_api_url:
            return DEFAULT_API_URL
        return self._hydrus_api_url

    @property
    def hydrus_auto_reconnect(self) -> bool:
        return self._hydrus_auto_reconnect

    @property
    def primary_tag_service(self) -> T.Optional[str]:
        if not self._primary_tag_service:
            return None
        return self._primary_tag_service

    @property
    def secondary_tag_services(self) -> T.Optional[list[str]]:
        if not self._secondary_tag_services:
            return []
        return self._secondary_tag_services

    @hydrus_access_key.setter
    def hydrus_access_key(self, value: str) -> None:
        self._hydrus_access_key = value
        self._set_str("HYDRUS", "access_key", self._hydrus_access_key)

    @hydrus_api_url.setter
    def hydrus_api_url(self, value: T.Optional[str]) -> None:
        self._hydrus_api_url = value
        self._set_str("HYDRUS", "api_url", self._hydrus_api_url)

    @hydrus_auto_reconnect.setter
    def hydrus_auto_reconnect(self, value: bool) -> None:
        self._hydrus_auto_reconnect = value
        self._set_str("HYDRUS", "auto_reconnect", str(self._hydrus_auto_reconnect))

    @primary_tag_service.setter
    def primary_tag_service(self, value: str) -> None:
        self._primary_tag_service = value
        self._set_str("LINTER", "primary_tag_service", self._primary_tag_service)

    @secondary_tag_services.setter
    def secondary_tag_services(self, value: list[str]) -> None:
        self._secondary_tag_services = value
        self._set_str("LINTER", "secondary_tag_services", json.dumps(self._secondary_tag_services))

    def save_to_file(self) -> None:
        if self.file_path.exists() and not self.file_path.is_file():
            #TODO: Notify the user that the config couldn't be saved.
            return

        # Create a deepcopy of the ConfigParser object without the empty sections and values
        cfg = deepcopy(self.config)
        for section_name in cfg.sections():
            section = cfg[section_name]

            for key, value in section.items():
                if not value:
                    section.pop(key)

        with open(self.file_path, "w") as configfile:
            cfg.write(configfile)

    def _load_from_file(self) -> ConfigParser:
        config = ConfigParser()
        for section in self._SECTIONS_TO_KEYS.keys():
            config.add_section(section)

        if self.file_path.exists() and self.file_path.is_file():
            config.read(self.file_path)

        return config

    # try to load a setting, with this order of priorities:
    # 1. get it from an environment variable
    # 2. get it from the configparser
    # 3. use a default value (fallback)
    def _from_env_or_config(self, section: str, key: str, env_key: str, fallback_value = None):
        if env_key is not None:
            env_val = os.getenv(env_key)
            if env_val is not None:
                return env_val

        value = self.config.get(section, key, fallback=None)
        # We take advantage of "" being falsy
        if value:
            return value
        return fallback_value


__settings: Settings = Settings()

# Simplified, we don't need much more.
def get_settings() -> Settings:
    return __settings
