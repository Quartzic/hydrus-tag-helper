import typing as T
import backend.actions
import backend.hydrus
import backend.rules
import backend.settings
import ui
import ui.dialogs
import ui.helper
import hydrus_api
from ui.helper import information_dialog, confirm_dialog, default_exceptions
import ui.widgets
from backend.actions import Action, TagAction
from backend.rules import Rule
from qtpy.QtCore import Qt
from qtpy.QtWebEngineWidgets import QWebEngineView
from qtpy.QtWidgets import QAction, QInputDialog, QLineEdit, QMenu
from ui.helper import information_dialog, confirm_dialog

class HomePage(ui.Page):
    def __init__(self) -> None:
        super().__init__("ui/templates/home.ui")

        self.rule_list = ui.widgets.RuleList()

        item = self.ui.layout().replaceWidget(self.ui.rule_list, self.rule_list)
        item.widget().deleteLater()

        self.menu_rules = QMenu("Rules")
        self.menu_rules.addAction("Reload", self._reload_rules)

        self.menu_hydrus = QMenu("Hydrus")
        action = self.menu_hydrus.addAction("Set up connection", lambda: ui.dialogs.HydrusConnectDialog())
        action.setShortcut("F9")
        self.menu_hydrus.addAction("About", self._show_about_hydrus)

        self.cached_rules = None

    def load(self) -> None:
        if self.cached_rules is None:
            self._reload_rule_cache()
        self.rule_list.update_content(self.cached_rules)

    @default_exceptions
    def _show_about_hydrus(self):
        ui.helper.information_dialog(None, "About Hydrus...", str(backend.hydrus.client().get_versioning()))

    def _reload_rules(self) -> None:
        self._reload_rule_cache()
        self.rule_list.update_content(self.cached_rules)

    def _reload_rule_cache(self) -> None:
        # TODO configurable rule file locations
        rule_file_locations = [ "./my_rules/" ]
        cached_rules = [*backend.rules.load_rules()]
        for location in rule_file_locations:
            cached_rules.extend(backend.rules.load_rules(location))
        self.cached_rules = cached_rules

    def get_menus(self) -> list[T.Union[QMenu, QAction]]:
        return [self.menu_rules, self.menu_hydrus]

class SlideshowPage(ui.Page):
    HTML_PRESET = '''
<html>
    <head>
        <style>
            body {
                margin: 0;
            }
            div#wrapper {
                width: 100vw;
                height: 100%;
                text-align: center;
                background-color: transparent;
            }
            div#wrapper img, div#wrapper video {
                display: block;
                position: absolute;
                margin: auto;
                inset: 0;
                max-width: 100%;
                max-height: 100%;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            REPLACEME
        </div>
    </body>
</html>
'''

    def __init__(self) -> None:
        super().__init__("ui/templates/slideshow.ui")
        self.rule: Rule = None
        self.current_idx = 0

        self.rule_actions = ui.widgets.RuleActions()
        self.rule_actions.action_triggered.connect(self.run_action)

        self._add_shortcut_action("left",  self.move_to_previous_file)
        self._add_shortcut_action("right", self.move_to_next_file)
        self._add_shortcut_action("home",  self.move_to_first_file)
        self._add_shortcut_action("end",   self.move_to_last_file)
        self._add_shortcut_action("esc",   ui.main_window().go_home)

        self.build_webview()

        item = self.ui.left_panel.layout().replaceWidget(self.ui.rule_actions, self.rule_actions)
        item.widget().deleteLater()

    def _add_shortcut_action(self, shortcut_str, trigger_func):
        the_action = QAction(shortcut_str, self)
        the_action.setShortcut(shortcut_str)
        the_action.triggered.connect(trigger_func)
        self.addAction(the_action)

    def build_webview(self) -> None:
        wv = QWebEngineView()
        wv.setContent(b"")
        self.web_page = wv.page()
        self.web_page.setBackgroundColor(Qt.transparent)
        #TODO: connect back/forward actions of the page to methods that update the index and values accordingly.

        # Ignore this, it's quick workaround to help with a thing I've been trying out in Qt Designer. - laure
        wv.setSizePolicy(self.ui.web_view.sizePolicy())

        item = self.ui.layout().replaceWidget(self.ui.web_view, wv)
        item.widget().deleteLater()
        self.ui.web_view = wv

    @default_exceptions
    def run_action(self, action: Action) -> None:
        success = False
        current_file = self.get_current_file()

        if current_file is None:
            # Act upon nothing - This can occur if the search is empty (there are no offending files)
            return

        # special exception for unskip
        if isinstance(action, backend.actions.UnskipAction):
            self.move_to_previous_file()
            return

        if isinstance(action, TagAction) and action.tag_dialog:
            title = "Apply Tags" if action.tag_dialog == "add" else "Remove Tags"

            text, ok = QInputDialog.getText(self, title, f"Insert tags to {action.tag_dialog}, comma separated.", QLineEdit.Normal)
            if ok:
                success = action.run(current_file.hash, text.split(","))
        else:
            success = action.run(current_file.hash)

        # after the action has run, we cannot assume our snapshot of the metadata is valid anymore
        self._invalidate_current_file_metadata()

        if success and action.resolves:
            self.move_to_next_file()

    def move_to_previous_file(self) -> None:
        self._move_to(self.current_idx - 1)

    def move_to_next_file(self) -> None:
        self._move_to(self.current_idx + 1)

    def move_to_first_file(self) -> None:
        self._move_to(0)

    def move_to_last_file(self) -> None:
        self._move_to(self.rule.search.result_count - 1)

    def _move_to(self, idx: int):
        max_idx = self.rule.search.result_count - 1
        self.current_idx = idx
        if self.current_idx < 0:
            self.current_idx = max_idx
        elif self.current_idx > max_idx:
            self.current_idx = 0
        self._load_current_file()

    @default_exceptions
    def _load_file_view(self, file : backend.hydrus.HydrusFile) -> None:
        mime = file.mime
        url = file.full_file_url

        if mime.startswith("image/"):
            self.web_page.setHtml(self.HTML_PRESET.replace("REPLACEME", f"<img src='{url}'>"))
        elif mime.startswith("video/"):
            #TODO: Make muted, and potentially others, configurable.
            self.web_page.setHtml(self.HTML_PRESET.replace("REPLACEME", f"<video autoplay loop controls muted src='{url}'>"))
        else:
            self.web_page.setHtml(self.HTML_PRESET.replace("REPLACEME", "The file could not be loaded (mimetype is not image or video)."))

    @default_exceptions
    def _load_tag_list(self, file : backend.hydrus.HydrusFile) -> None:
        # The primary tag service should always be present; the secondary tag services list may either have services or be empty.
        services = [backend.settings.get_settings().primary_tag_service] + backend.settings.get_settings().secondary_tag_services
        # TODO: configure the statuses to check
        statuses = [backend.hydrus.TAG_STATUS_CURRENT]
        tags = file.get_tags(services, statuses)
        self.ui.tag_list.clear()
        for tag_str in tags:
            tag_item = ui.widgets.TagListWidgetItem(tag_str)
            self.ui.tag_list.addItem(tag_item)

    @default_exceptions
    def _load_current_file(self) -> None:
        file = self.get_current_file()

        if file is None:
            return

        self.rule_actions.update_buttons(file.is_in_inbox, file.is_in_trash, False)

        self._load_file_view(file)
        self._load_tag_list(file)

    def get_current_file(self):
        if self.rule.search.empty:
            return None
        return self.rule.search.get_file(self.current_idx)

    def _invalidate_current_file_metadata(self) -> None:
        file = self.get_current_file()
        if file is None: return
        self.rule.search.invalidate_file_metadata(self.current_idx)
        self.rule_actions.update_buttons(file.is_in_inbox, file.is_in_trash, False)
        self._load_tag_list(file)

    def unload(self) -> None:
        self.current_idx = 0
        # We have to replace the entire webview every time to avoid a flicker with the last rendered frame.
        # This could be avoided if we find a way to force that frame clearing when leaving the page.
        self.build_webview()

    def load(self) -> None:
        self.rule_actions.update_content([*self.rule.actions, *self.rule.custom_actions])
        self.ui.rule_name.setText(self.rule.name)
        self.ui.rule_desc.setText(self.rule.desc)
        self._load_current_file()

class SettingsPage(ui.Page):
    def __init__(self) -> None:
        super().__init__("ui/templates/settings.ui")

        settings = backend.settings.get_settings()

        self.ui.access_key.setText(settings.hydrus_access_key)

        # Messy, but works.
        self.ui.api_url.setPlaceholderText(backend.hydrus.DEFAULT_API_URL)
        api_url = settings.hydrus_api_url
        if api_url != backend.hydrus.DEFAULT_API_URL:
            self.ui.api_url.setText(api_url)

        self.ui.auto_reconnect.setChecked(settings.hydrus_auto_reconnect)

        self.ui.auto_reconnect.toggled.connect(self._toggle_auto_reconnect)
        self.ui.connect_button.clicked.connect(self._connect_to_hydrus_and_save_credentials)

    def load(self) -> None:
        # TODO: load values for widgets here instead of on page creation.
        # Then add a button to save changes instead of saving automatically or when the
        # connection is succesful.
        pass

    def unload(self) -> None:
        # TODO: add confirmation dialog to save changes, once the above TO-DO is fullfilled.
        pass

    def _toggle_auto_reconnect(self, checked: bool) -> None:
        settings = backend.settings.get_settings()

        settings.hydrus_auto_reconnect = checked

        settings.save_to_file()

    def _connect_to_hydrus_and_save_credentials(self) -> None:
        access_key = self.ui.access_key.text()
        api_url = self.ui.api_url.text()

        if api_url == "":
            api_url = None

        if access_key == "":
            access_key = self.request_hydrus_api_key(api_url)
            if access_key is None:
                return

        try:
            backend.hydrus.client().connect(access_key, api_url)
        except backend.hydrus.HydrusConnectionError as e:
            ui.helper.error_connecting_dialog(None, str(e))
            return

        client = backend.hydrus.client()
        message = f"A connection with the Client API was established.\n\n{client.get_versioning()}"

        information_dialog(self, "Success!", message)

        # Only overwrite stored values if the connection was succesful.
        settings = backend.settings.get_settings()

        settings.hydrus_access_key = access_key
        settings.hydrus_api_url = api_url
        settings.hydrus_auto_reconnect = self.ui.auto_reconnect.isChecked()

        settings.save_to_file()

    def request_hydrus_api_key(self, api_url) -> T.Optional[str]:
        if confirm_dialog(self, "Request API Key",
            '1. Go to the Hydrus Client.\n'
            '2. Navigate to "services->review services->local->client api"\n'
            '3. Click "add->from api request".\n'
            '4. Click OK when done.'):
            try:
                access_key = backend.request_api_key(api_url)
                information_dialog(self, "Connection Success.", "Confirm the permissions in the hydrus dialog, then press OK here.")
                return access_key
            except hydrus_api.ConnectionError:
                information_dialog(self, "Connection Error.", "Make sure the Client API is running and that the selected port is correct.")
        return None
