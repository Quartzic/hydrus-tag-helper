from qtpy.QtCore import Qt
from qtpy.QtGui import QFont
from qtpy.QtWidgets import QWidget, QLabel, QSizePolicy, QMessageBox
import traceback
import backend.hydrus
import hydrus_api

Align = Qt.AlignmentFlag
SizePolicy = QSizePolicy.Policy
StandardButton = QMessageBox.StandardButton

def loadUi(ui_file_path: str, base_class: type[QWidget]):
    import qtpy.uic

    ui = qtpy.uic.loadUi(ui_file_path)
    if not isinstance(ui, base_class):
        raise ValueError(f'The root element of "{ui_file_path}" should be "{base_class.__name__}", not "{type(ui).__name__}"')

    return ui

def information_dialog(parent: QWidget, text: str, informative_text: str) -> None:
    mbox = QMessageBox(parent)
    mbox.setText(text)
    mbox.setInformativeText(informative_text)
    mbox.setStandardButtons(StandardButton.Ok)
    mbox.setDefaultButton(StandardButton.Ok)
    mbox.setEscapeButton(StandardButton.Ok)
    mbox.exec()

def confirm_dialog(parent: QWidget, text: str, informative_text: str, default_ok = True) -> bool:
    mbox = QMessageBox(parent)
    mbox.setText(text)
    mbox.setInformativeText(informative_text)
    mbox.setStandardButtons(StandardButton.Ok | StandardButton.Cancel)
    mbox.setDefaultButton(StandardButton.Ok if default_ok else StandardButton.Cancel)
    mbox.setEscapeButton(StandardButton.Cancel)
    return mbox.exec() == StandardButton.Ok

def error_connecting_dialog(parent: QWidget, debug_message : str) -> None:
    information_dialog(parent, "Error establishing connection", debug_message)

# to be replaced by something better :)
def error_dialog(parent: QWidget, title, message) -> None:
    information_dialog(parent, title, message)

def fatal_error_dialog(parent: QWidget, debug_message : str) -> None:
    information_dialog(parent, "Fatal error", "There was an uncaught exception: " + debug_message)

def new_label(text: str, center: bool = False, word_wrap: bool = True, font_size: int = 12) -> QLabel:
    label = QLabel(text=text)
    label.setWordWrap(word_wrap)
    if center:
        label.setAlignment(Align.AlignCenter)

    font = QFont()
    font.setPointSize(font_size)
    label.setFont(font)
    return label

# wrapper that handles otherwise uncaught exceptions
def default_exceptions(func, parent : QWidget = None):
    def wrap_func(*args, **kwargs):
        try:
            func(*args, **kwargs)

        except backend.hydrus.HydrusConnectionError as e:
            error_connecting_dialog(parent, str(e))

        # while in theory hydrus_api.ConnectionError is wrapped & handled by
        # our centralized API module, in practice if the user does some
        # *really* strange things then this can leak through.
        except hydrus_api.ConnectionError as e:
            error_connecting_dialog(parent, str(e))

        except Exception as e:
            traceback.print_exception(e)
            message_lines = [
                "An unexpected/uncaught exception occurred. Please report this as a bug. You may continue using the app, but I recommend closing it and restarting to be safe.",
                "",
                "Error message:",
                f"{type(e).__name__}: {str(e)}"
            ]
            error_dialog(parent, "Error", '\n'.join(message_lines))

    return wrap_func
