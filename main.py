import backend.settings
import backend.hydrus
import ui
import sys
import traceback
import qtpy
from ui.dialogs import HydrusConnectDialog
from qtpy.QtCore import Qt
from qtpy.QtWidgets import QApplication, QMessageBox

def main():
    try:
        settings = backend.settings.get_settings()

        qt_api_version = qtpy.PYSIDE_VERSION if qtpy.PYSIDE_VERSION else qtpy.PYQT_VERSION
        print(f"QtPy: Qt {qtpy.QT_VERSION}", f"with {qtpy.API_NAME} ({qt_api_version})")

        QApplication.setAttribute(Qt.ApplicationAttribute.AA_ShareOpenGLContexts)
        app = QApplication(sys.argv)

        if settings.hydrus_auto_reconnect:
            if settings.hydrus_access_key is None:
                HydrusConnectDialog()

            # We have to check again in case the user has skipped the previous dialog.
            if settings.hydrus_access_key is not None:
                try:
                    backend.hydrus.client().connect()
                except backend.hydrus.HydrusConnectionError as e:
                    # TODO: Make a HydrusConnectionError dialog with a retry button.
                    QMessageBox.warning(
                        None,
                        "About connecting...",
                        f"About connecting...\n\nI was unable to connect to the client API automatically. Please go to Hydrus->Set up connection.\n\nError message: {str(e)}")

        if not settings.primary_tag_service:
            # TODO: Open a dialog to select a tag service.
            # It will ask hydrus if the tag service exists before proceeding, if the credentials have been set above.
            pass

        ui.create_main_window()

        sys.exit(app.exec_())

    except Exception as e:
        traceback.print_exception(e)
        ui.helper.fatal_error_dialog(None, str(e))
        sys.exit(1)


if __name__ == "__main__":
    main()
