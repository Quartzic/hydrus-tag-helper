import re
import functools
import hydrus_api
import hydrus_api.utils
import typing as T
from . import settings

# The goal of this module is to centralize all hydrus api-related functionality
# into one place. Whenever possible, I am going to use aliases for constants
# already defined in the hydrus_api module. That way, we don't have to import
# hydrus_api anywhere that references these constants. -- cinnamon

DEFAULT_API_URL = hydrus_api.DEFAULT_API_URL

TAG_STATUS_CURRENT    = hydrus_api.TagStatus.CURRENT
TAG_STATUS_PENDING    = hydrus_api.TagStatus.PENDING
TAG_STATUS_DELETED    = hydrus_api.TagStatus.DELETED
TAG_STATUS_PETITIONED = hydrus_api.TagStatus.PETITIONED

TAG_ACTION_ADD              = hydrus_api.TagAction.ADD
TAG_ACTION_DELETE           = hydrus_api.TagAction.DELETE
TAG_ACTION_PEND             = hydrus_api.TagAction.PEND
TAG_ACTION_RESCIND_PENDING  = hydrus_api.TagAction.RESCIND_PENDING
TAG_ACTION_PETITION         = hydrus_api.TagAction.PETITION
TAG_ACTION_RESCIND_PETITION = hydrus_api.TagAction.RESCIND_PETITION

# List of default namespace colors.
# We cannot ask Hydrus for the user's preferred colors (yet?)
# see: https://github.com/hydrusnetwork/hydrus/issues/360
NAMESPACE_COLOR_OTHER = "#72A0C1"
NAMESPACE_COLOR_NONE = "#006FFA"
NAMESPACE_COLORS = {
    "artist":    "#AA0000",
    "character": "#00AA00",
    "copyright": "#AA00AA",
    "creator":   "#AA0000",
    "meta":      "#000000",
    "person":    "#008000",
    "series":    "#AA00AA",
    "studio":    "#800000",
    "system":    "#996515",
}

_APP_NAME = "hydrus tag helper"

_APP_PERMISSIONS = [
    # The IMPORT_FILES permission enables moving files to/from inbox, archive,
    # and trash. This program won't attempt to import *new* files to the
    # instance, it will only move files around that were already imported.
    hydrus_api.Permission.IMPORT_FILES,
    hydrus_api.Permission.SEARCH_FILES,
    hydrus_api.Permission.ADD_TAGS
]

def join_tag(the_split_tag : tuple[T.Optional[str], str]):
    if not the_split_tag[0]:
        return the_split_tag[1]
    return f"{the_split_tag[0]}:{the_split_tag[1]}"

# Splits the tag into namespace and subtag.
def split_tag(tag_str: str) -> tuple[T.Optional[str], str]:
    parts = tag_str.split(':', 1)
    if len(parts) > 1:
        return (parts[0], parts[1])
    else:
        return (None, parts[0])

def compare_split_tags(tag1 : tuple[T.Optional[str], str], tag2 : tuple[T.Optional[str], str]):
    # namespace tags go above nonnamespaced tags
    if not tag1[0] and tag2[0]:
        return 1
    if tag1[0] and not tag2[0]:
        return -1

    # if both tags are namespaced, then compare by namespace first
    if tag1[0] and not tag2[0]:
        if tag1[0] < tag2[0]: return -1
        if tag1[0] > tag2[0]: return 1

    # compare by subtag if tags belong to the same namespace or they are nonnamespaced
    if tag1[1] < tag2[1]: return -1
    if tag1[1] > tag2[1]: return 1
    return 0

def compare_tags(tag1 : str, tag2 : str):
    return compare_split_tags(split_tag(tag1), split_tag(tag2))

def sort_tag_list(tags : T.Iterable, reverse = False):
    sorted(tags, reverse=reverse, key=functools.cmp_to_key(compare_tags))
    return tags

def sort_split_tag_list(tags : T.Iterable, reverse = False):
    sorted(tags, reverse=reverse, key=functools.cmp_to_key(compare_split_tags))
    return tags

# don't call this directly, as in the future there might be an API method for this
def _get_namespace_color(namespace: str):
    if namespace is None:
        return NAMESPACE_COLOR_NONE
    else:
        return NAMESPACE_COLORS.get(namespace, NAMESPACE_COLOR_OTHER)

# don't call this directly, as in the future there might be an API method for this
def _get_tag_color(tag_str: str):
    namespace, subtag_discard = split_tag(tag_str)
    return _get_namespace_color(namespace)


# Thrown when we fail to connect to the client API
# This error should NOT be fatal!
class HydrusConnectionError(RuntimeError):
    def __init__(self, msg):
        super().__init__(msg)

class HydrusVersionInfo:
    def __init__(
            self,
            api_module_version : int,
            api_version : int,
            client_version : int):
        self.api_module_version = api_module_version
        self.api_version = api_version
        self.client_version = client_version

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        parts = [
            f"Client version: {self.client_version}",
            f"API version: {self.api_version}",
            f"API module version: {self.api_module_version}"
        ]
        return '\n'.join(parts)

# thin wrapper around connection
class Hydrus:
    def __init__(self):
        self._client : hydrus_api.Client = None

    @property
    def connected(self):
        return self._client is not None

    def connect(
            self,
            access_key : str = None,
            api_url : str = None):

        if access_key is None:
            access_key = settings.get_settings().hydrus_access_key

        if api_url is None:
            api_url = settings.get_settings().hydrus_api_url

        try:
            if access_key is not None:
                self._client = hydrus_api.Client(access_key, api_url)

                if not hydrus_api.utils.verify_permissions(self._client, _APP_PERMISSIONS):
                    self._client = None
                    raise HydrusConnectionError(
                        "The provided API key does not have all of the required permissions. "
                        "Please make sure it has: IMPORT_FILES, SEARCH_FILES, ADD_TAGS")

                # save these for later, since we know they work now
                self.api_url = api_url
                self.access_key = access_key

        except hydrus_api.ConnectionError as e:
            self._client = None
            raise HydrusConnectionError("Connection to the client was refused - Is hydrus open?")
        except hydrus_api.APIError as e:
            self._client = None
            raise HydrusConnectionError("An API error occurred while connecting to hydrus: " + str(e))

    # don't connect if we don't need to :)
    def lazy_connect(self):
        if not self.connected:
            self.connect()

    def request_api_key(self, api_url: str = None) -> str:
        if api_url is None:
            api_url = settings.get_settings().hydrus_api_url

        return hydrus_api.Client(api_url=api_url).request_new_permissions(_APP_NAME, _APP_PERMISSIONS)

    def get_tag_color(self, tag_str: str):
        return _get_tag_color(tag_str)

    def get_namespace_color(self, tag_str: str):
        return _get_namespace_color(tag_str)

    def get_api_and_hydrus_version(self):
        self.lazy_connect()
        x = self._client.get_api_version()
        if x is None:
            return (None, None)
        return (x.get('version'), x.get('hydrus_version'))

    def get_versioning(self):
        self.lazy_connect()
        hydrus_dict = self._client.get_api_version()
        return HydrusVersionInfo(
            api_module_version = self._client.VERSION,
            api_version = hydrus_dict.get('version'),
            client_version = hydrus_dict.get('hydrus_version'))

    def change_tags(
            self,
            hashes,
            service_names_to_actions_to_tags):
        self.lazy_connect()
        self._client.add_tags(
            hashes=hashes,
            service_names_to_actions_to_tags=service_names_to_actions_to_tags)

    def delete_files(self, hashes):
        self.lazy_connect()
        return self._client.delete_files(hashes)

    def unarchive_files(self, hashes):
        self.lazy_connect()
        return self._client.unarchive_files(hashes)

    def archive_files(self, hashes):
        self.lazy_connect()
        return self._client.archive_files(hashes)

    def search_files(self, query):
        self.lazy_connect()
        return self._client.search_files(query)

    def get_raw_file_metadata(self, file_ids = None, hashes = None):
        self.lazy_connect()
        return self._client.get_file_metadata(
            file_ids = file_ids,
            hashes = hashes)

    def get_files_by_id(self, file_ids, download_metadata_now = False):
        if not download_metadata_now:
            return [HydrusFile(x) for x in file_ids]

        return [HydrusFile(x) for x in self.get_raw_file_metadata(file_ids = file_ids)]

    def _get_file_url_by_hash(self, the_hash : str, thumbnail: bool = False):
        # if api_url ends in / and _GET_FILE_PATH begins with /, then the URL becomes invalid
        clean_api_url = re.sub(r'\/*$', "", self.api_url)

        self.lazy_connect()
        path = self._client._GET_THUMBNAIL_PATH if thumbnail else self._client._GET_FILE_PATH

        url = clean_api_url + path + f"?hash={the_hash}&Hydrus-Client-API-Access-Key={self.access_key}"
        return url

    def get_full_file_url_by_hash(self, the_hash : str):
        return self._get_file_url_by_hash(the_hash)

    def get_thumbnail_url_by_hash(self, the_hash : str):
        return self._get_file_url_by_hash(the_hash, thumbnail=True)

_hydrus_instance : Hydrus = Hydrus()

def client() -> Hydrus:
    return _hydrus_instance

# thin wrapper around hydrus file metadata object
class HydrusFile:
    def __init__(self, init_data):
        if isinstance(init_data, int):
            self._file_id = init_data
            self._cached_metadata = None
        elif isinstance(init_data, dict):
            if not "file_id" in init_data:
                raise ValueError("HydrusFile initialization dict is missing one or more keys")
            self._file_id = init_data["file_id"]
            self._cached_metadata = init_data
        else:
            raise TypeError("Cannot initialize HydrusFile from type " + type(init_data).__name__)

    def invalidate_cache(self):
        self._cached_metadata = None

    def _download_metadata(self):
        self._cached_metadata = client().get_raw_file_metadata([self._file_id])[0]

    @property
    def is_metadata_cached(self):
        return self._cached_metadata is not None

    def lazy_download_metadata(self):
        if not self.is_metadata_cached:
            self._download_metadata()

    @property
    def file_id(self):
        return self._file_id

    def _metadata_key(self, key : str, default = None):
        self.lazy_download_metadata()
        return self._cached_metadata.get(key, default)

    @property
    def hash(self):
        return self._metadata_key("hash")

    @property
    def size(self):
        return self._metadata_key("size")

    @property
    def mime(self):
        return self._metadata_key("mime")

    @property
    def extension(self):
        return self._metadata_key("ext")

    @property
    def width(self):
        return self._metadata_key("width")

    @property
    def height(self):
        return self._metadata_key("height")

    @property
    def duration(self):
        return self._metadata_key("duration")

    @property
    def time_modified(self):
        return self._metadata_key("time_modified")

    @property
    def has_audio(self):
        return self._metadata_key("has_audio")

    @property
    def num_frames(self):
        return self._metadata_key("num_frames")

    @property
    def num_words(self):
        return self._metadata_key("num_words")

    @property
    def is_in_inbox(self):
        return self._metadata_key("is_inbox")

    @property
    def is_in_archive(self):
        return not self.is_in_inbox

    @property
    def is_in_trash(self):
        return self._metadata_key("is_trashed")

    @property
    def known_urls(self):
        return self._metadata_key("known_urls")

    @property
    def raw_metadata(self):
        self.lazy_download_metadata()
        return self._cached_metadata

    @property
    def full_file_url(self):
        return client().get_full_file_url_by_hash(self.hash)

    @property
    def thumbnail_url(self):
        return client().get_thumbnail_url_by_hash(self.hash)

    def move_to_archive(self):
        client().archive_files([self.hash])

    def move_to_inbox(self):
        client().unarchive_files([self.hash])

    def move_to_trash(self):
        client().delete_files([self.hash])

    def get_tags(
            self,
            service_names: list[str],
            statuses: hydrus_api.TagStatus = [TAG_STATUS_CURRENT]):

        sntstt: dict = self._metadata_key("service_names_to_statuses_to_tags")

        if sntstt is None:
            return []

        ret = set()
        for service in service_names:
            stt = sntstt.get(service)
            if stt is None:
                continue
            for status in statuses:
                ret.update(stt.get(str(status), []))

        return list(ret)

