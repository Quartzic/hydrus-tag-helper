import typing as T
from backend.rules import Rule
from qtpy.uic import loadUi
from qtpy.QtWidgets import QStackedWidget, QBoxLayout, QWidget, QLayout, QMainWindow, QMenu, QAction, QMenuBar

class BoxLayout(QBoxLayout):
    def __init__(self, direction: QBoxLayout.Direction) -> None:
        super().__init__(direction)
        self.setContentsMargins(0, 0, 0, 0)

    def remove_all_items(self):
        for i in reversed(range(self.count())):
            item = self.itemAt(i)
            widget = item.widget()
            layout = item.layout()
            if widget: widget.deleteLater()
            if layout: layout.deleteLater()
            self.removeItem(item)

    def add(self, item: T.Union[QWidget, QLayout], stretch: int = 0) -> None:
        if isinstance(item, QLayout):
            self.addLayout(item, stretch=stretch)
        else:
            self.addWidget(item, stretch=stretch)

class VBoxLayout(BoxLayout):
    def __init__(self) -> None:
        super().__init__(QBoxLayout.Direction.TopToBottom)

class HBoxLayout(BoxLayout):
    def __init__(self) -> None:
        super().__init__(QBoxLayout.Direction.LeftToRight)

class Page(QWidget):
    def __init__(self, ui_file_path: str) -> None:
        super().__init__()
        self.setLayout(HBoxLayout())
        self.setMinimumSize(640, 480)

        self._ui = loadUi(ui_file_path)

        self.layout.add(self.ui)

    @property
    def ui(self) -> QWidget:
        return self._ui

    @property
    def layout(self) -> BoxLayout:
        return super().layout()

    def unload(self) -> None:
        pass

    def load(self) -> None:
        pass

    def get_menus(self) -> list[T.Union[QMenu, QAction]]:
        return []

class MainWindow(QMainWindow):
    def __init__(self) -> None:
        super().__init__()

        self.setWindowTitle("Hydrus Tag Helper")
        self.resize(800, 600)
        self.setMinimumSize(640, 480)

        self.stack = QStackedWidget()
        self.setCentralWidget(self.stack)

    def update_menu_bar(self) -> None:
        bar = QMenuBar(self)

        bar.addMenu(self._create_nav_menu(bar))

        for i in self.current_page.get_menus():
            if isinstance(i, QMenu):
                bar.addMenu(i)
            else:
                bar.addAction(i)

        self.setMenuBar(bar)

    def _create_nav_menu(self, bar: QMenuBar) -> QMenu:
        from ui.pages import HomePage, SettingsPage
        nav = QMenu("Navigate", bar)

        if not isinstance(self.current_page, HomePage):
            nav.addAction("Home").triggered.connect(self.go_home)

        #TODO: Temporarily disabled, as the hydrus credentials can now be set from a dialog.
        # if not isinstance(self.current_page, SettingsPage):
        #     nav.addAction("Settings").triggered.connect(self.go_settings)

        self.nav_separator = nav.addSeparator()
        nav.addAction("Exit").triggered.connect(self.close)

        return nav

    @property
    def current_page(self) -> Page:
        return self.stack.currentWidget()

    def add_default_pages(self) -> None:
        from ui.pages import HomePage, SettingsPage, SlideshowPage

        self.stack.addWidget(HomePage())
        self.stack.addWidget(SettingsPage())
        self.stack.addWidget(SlideshowPage())

        self.go_home()

    def go_home(self) -> None:
        self._go_to_page(0)

    def go_settings(self) -> None:
        self._go_to_page(1)

    def go_to_rule_page(self, rule: Rule) -> None:
        self.stack.widget(2).rule = rule
        self._go_to_page(2)

    def _go_to_page(self, index: int) -> None:
        self.current_page.unload()
        self.stack.setCurrentIndex(index)
        self.update_menu_bar()
        self.current_page.load()



__main_window = None
def create_main_window() -> MainWindow:
    global __main_window
    if __main_window is not None: raise RuntimeError("A MainWindow already exists!")
    __main_window = MainWindow()
    __main_window.add_default_pages()
    __main_window.show()
    return __main_window

def main_window() -> MainWindow:
    if __main_window is None: raise RuntimeError("MainWindow is not defined yet! Use ui.create_main_window().")
    return __main_window